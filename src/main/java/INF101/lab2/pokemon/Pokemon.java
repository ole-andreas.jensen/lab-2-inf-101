package INF101.lab2.pokemon;

import java.lang.Math;
import java.util.Random;



// Oppgave 1; deloppgave A; feltvariabler

public class Pokemon implements IPokemon {
    String name;                                        // Dette er FELTVARIABLER 
    int healthPoints;
    int maxHealthPoints;
    int strength;
    Random random;
   

// Oppgave 1; deloppgave B; konstruktører

    public Pokemon(String name) {              // her er konstruktørene. Den initierer feltvariablene.
        this.name = name;
        this.random = new Random();
        this.healthPoints = (int) Math.abs(Math.round(100 + 10 * random.nextGaussian()));
        this.maxHealthPoints = this.healthPoints;
        this.strength = (int) Math.abs(Math.round(20 + 10 * random.nextGaussian()));

    }

    public String getName() {
        return this.name;
    }

    @Override
    public int getStrength() {
        return this.strength;
    }

    @Override
    public int getCurrentHP() {
        return this.healthPoints;
    }

    @Override
    public int getMaxHP() {
        return this.maxHealthPoints;
    }

    public boolean isAlive() {
        if (healthPoints > 0) {         
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public void attack(IPokemon target) {
        int damageInflicted = (int) (this.strength + this.strength / 2 * random.nextGaussian());
        System.out.println(this.name + " attacks " + target.getName() + ".");
        target.damage(damageInflicted);
        if (!target.isAlive()) {
            System.out.println(target.getName() + " is defeated by " + this.name);
            
        }
    }

    @Override
    public void damage(int damageTaken) {
        if (damageTaken >= 0) {
            if (damageTaken > healthPoints) {
                healthPoints = 0;
            }
            else healthPoints = healthPoints - damageTaken;
        }
        System.out.println(name + " takes " + damageTaken + " damage and is left with " + healthPoints + "/" + maxHealthPoints + " HP");
    }

    @Override
    public String toString() {
        String mewStreng = (name + " HP: (" + healthPoints + "/" + maxHealthPoints + ") STR: " + strength); 
            return mewStreng;

        }
    }


